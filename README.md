# Water Bottle Problem

Calculates the number of steps it takes to solve the water bottle problem.

When running you will be promted to enter the values to test. Enter in the form of three integers (separated by a space) with the size of the large bottle first, the small bottle second and the desired target size last.

---

## Requirements

- Node 10.x
- NPM 6.x

---

## Run

The applicaton uses a Makefile to install and run. The following commands are available:

`make install` - Install dependencies

`make first-run` - Install dependencies and run application

`make run` - Run application

`make tests-unit` - Run unit tests

`make clean` - Clean up dependencies
