'use strict';

const { parseUserInput, calculateSteps } = require('./src/calculations');

const userInput = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout,
});

userInput.question('Enter bottle sizes and target as three integers ([large bottle] [small bottle] [target]): ', (input) => {
    const inputs = input.split(' ');

    if (inputs.length !== 3) {
        console.log('You need to enter three values.');
	}

	const userInputs = parseUserInput(inputs);
	const numberOfSteps = calculateSteps(...userInputs);

    if (numberOfSteps === 0) {
		console.log(`Simon says: This problem cannot be solved.`);
	} else if (numberOfSteps === 100) {
		console.log(`Simon says: It would take more than 100 steps to solve this problem. I gave up.`);
	} else {
		console.log(`Simon says: It takes ${numberOfSteps} steps to solve the problem.`);
	}

    userInput.close();
});
