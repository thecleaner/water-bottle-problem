'use strict';

const expect = require('chai').expect;
const { isSolvable, parseUserInput, pouring, calculateSteps } = require('../../src/calculations');

describe('Parse user input', () => {
	it('should return same order when valid', () => {
		expect(parseUserInput([5,3,4])).to.eql([5,3,4]);
	});
	it('should always return large bottle first', () => {
		expect(parseUserInput([3,5,4])).to.eql([5,3,4]);
	});
	it('should return 0 for non numerical values', () => {
		expect(parseUserInput(['apan',5,4])).to.eql([5,0,4]);
	});
	it('should convert floats to integers', () => {
		expect(parseUserInput([8.3,'5,6',4])).to.eql([8,5,4]);
	});
});

describe('Check if problem is solvable', () => {
	it('should return true when target is divisible by bottles gcd', () => {
		expect(isSolvable(5,3,4)).to.eql(true);
	});
	it('should return false when target is not divisible by bottles gcd', () => {
		expect(isSolvable(9,6,5)).to.eql(false);
	});
});

describe('Fill large or small bottle first', () => {
	it('should take 6 steps to get 4 litres with large bottle', () => {
		expect(pouring(5,3,4)).to.eql(6);
	});
	it('should take 8 steps to get 4 litres with small bottle', () => {
		expect(pouring(3,5,4)).to.eql(8);
	});
	it('should take 8 steps to get 1 litre with large bottle', () => {
		expect(pouring(5,3,1)).to.eql(8);
	});
	it('should take 4 steps to get 1 litre with small bottle', () => {
		expect(pouring(3,5,1)).to.eql(4);
	});
});

describe('Calculate number of steps', () => {
    it('should return 6 steps to get 4 litres', () => {
        expect(calculateSteps(5,3,4)).to.eql(6);
    });
    it('should return 4 steps to get 1 litre', () => {
        expect(calculateSteps(5,3,1)).to.eql(4);
	});
	it('should return 1 step when target is the same as bottle', () => {
		expect(calculateSteps(5,3,3)).to.eql(1);
	});
	it('should return 0 when not solvable', () => {
		expect(calculateSteps(9,6,5)).to.eql(0);
	});
});
