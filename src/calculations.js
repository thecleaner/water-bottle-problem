'use strict';

const isSolvable = (bottle1, bottle2, target) => {
	const calculateDivisor = (x, y) => {
		if (!y) {
			return x;
		}
		return calculateDivisor(y, x % y);
	};

	const bottlesDivisor = calculateDivisor(bottle1, bottle2);

	if (target % bottlesDivisor === 0) {
		return true;
	} else {
		return false;
	}
}

const parseUserInput = (inputArray) => {
	const bottle1 = parseInt(inputArray[0]);
    const bottle2 = parseInt(inputArray[1]);
	const target = parseInt(inputArray[2]);

	let largeBottle = bottle1 ? bottle1 : 0;
	let smallBottle = bottle2 ? bottle2 : 0;
	const targetOut = target ? target : 0;

	if (smallBottle > largeBottle) {
		[largeBottle, smallBottle] = [smallBottle, largeBottle];
	}

	return [largeBottle, smallBottle, targetOut];
}

const pouring = (pourFromBottle, pourToBottle, target) => {
	let step = 0;
	let pourFromAmount = pourFromBottle;
	let pourToAmount = 0;
	let pourAmount = 0;

	while (pourFromAmount !== target && pourToAmount !== target) {
		pourAmount = (pourFromAmount < (pourToBottle - pourToAmount)) ? pourFromAmount : (pourToBottle - pourToAmount);

		pourFromAmount -= pourAmount;
		pourToAmount += pourAmount;
		step++;

		if (pourFromAmount === 0) {
            pourFromAmount = pourFromBottle;
            step++;
        }

        if (pourToAmount === pourToBottle) {
            pourToAmount = 0;
            step++;
		}

		if (step > 99) {
			break;
		}
	}

	return step;
}

const calculateSteps = (largeCapacity, smallCapacity, target) => {
	if (isSolvable(largeCapacity, smallCapacity, target) === false) {
		return 0;
	}

	if (largeCapacity === target || smallCapacity === target) {
		return 1;
	}

	const fillLargeFirstSteps = pouring(largeCapacity, smallCapacity, target);
	const fillSmallFirstSteps = pouring(smallCapacity, largeCapacity, target);

    return (fillLargeFirstSteps < fillSmallFirstSteps) ? fillLargeFirstSteps : fillSmallFirstSteps;
}

module.exports = {
	isSolvable,
	parseUserInput,
	pouring,
    calculateSteps
};
