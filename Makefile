install:
	@npm install

first-run: install run

run:
	@node app.js

tests-unit:
	npm run tests-unit

clean:
	@rm -rf node_modules
